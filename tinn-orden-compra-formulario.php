<?php
/**
 * Plugin Name:  TINN Conector JIRA
 * Description:  Plugin para consultar una Orden de Compra (Issues) en la Plataforma Jira. Utiliza el shortcode [tinn_jira_form]
 * Version:      0.1.1
 * Author:       Oscar Velasquez
 * Author URI:   http://somostinn.website/
 * PHP Version:  5.6
 *
 * @category Form
 * @package  TINN
 * @author   Oscar Velasquez <oscar.somostinn@gmail.com>
 * @license  GPLv2 http://www.gnu.org/licenses/gpl-2.0.txt
 * @link     http://somostinn.website
 */

register_activation_hook(__FILE__, 'TINN_Orden_Compra_init');

/**
 * Realiza las acciones necesarias para configurar el plugin cuando se activa
 *
 * @return void
 */
function TINN_Orden_Compra_init()
{
    global $wpdb;
    $tabla_jira_oc = $wpdb->prefix . "jira_oc";
    $charset_collate = $wpdb->get_charset_collate();
    $query = "CREATE TABLE IF NOT EXISTS $tabla_jira_oc (
		id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
		orden_compra VARCHAR(40) NOT NULL,
    tipo VARCHAR(13) NOT NULL,
    nombre VARCHAR(25) NOT NULL,
    apellido VARCHAR(25) NOT NULL,
    email VARCHAR(100) NOT NULL,
    banco VARCHAR(50),
    transferencia VARCHAR(50),
    imagen VARCHAR(255),
    fecha datetime,
		ip VARCHAR(40),
		code VARCHAR(100),
		created_at datetime NOT NULL,
		UNIQUE (id)
	) $charset_collate;";

    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($query);
}

add_shortcode('tinn_jira_form', 'TINN_Jira_form');

/**
 * Crea y procesa el formulario de Orden de Compra
 *
 * @return string
 */
function TINN_Jira_form()
{
    if (!empty($_POST))
    {
      if ( !empty( $_POST['ordenCompra'] ) && isset( $_POST['ordenCompra'] ) && isset ( $_POST['orden_compra_nonce'] ) )
      {
            $ordenCompra = sanitize_text_field($_POST['ordenCompra']);

            //Consultar en Jira la Orden de Compra
            $ordenesCompras = TINN_Conexion_Jira($ordenCompra);

            //Orden de Compra Válida
            if ($ordenesCompras['code'] === 200) {
                //Mostrar Formulario de Pago
                TINN_Formulario_Orden_Compra_Pagalo_Facil($ordenesCompras);
            } else
            {
                //Mostrar Información de Orden de Compra No Válida
                TINN_Formulario_Resultado_Jira_Orden_Compra_No_Valida();
            }


        } elseif ( !empty( $_POST['graba_orden_compra_pago'] ) && isset( $_POST['graba_orden_compra_pago'] ) && isset ( $_POST['orden_compra_pago_nonce'] ) )
        {
                //PROCESAR PAGO - PAGALO FACIL
                TINN_Pagalo_Facil($_POST);

        } else
        {
          if ( !empty( $_POST['graba_orden_compra_transferencia'] ) && isset( $_POST['graba_orden_compra_transferencia'] ) && isset ( $_POST['orden_compra_transferencia_nonce'] ) ) {
            // PROCESAR PAGO - TRANSFERENCIA
            TINN_Transferencia($_POST);
          }
      }

    } else {
        //Mostrar Formulario para capturar Ordenes de Compra
        TINN_Formulario_Orden_Compra();
    }

    return ob_get_clean();
}

add_action("admin_menu", "TINN_Orden_Compra_menu");



/**
 * Agrega el menú del plugin al formulario de WordPress
 *
 * @return void
 */
function TINN_Orden_Compra_menu()
{
    add_menu_page(
        "Ordenes de Compras",
        "Orden de Compra",
        "manage_options",
        "TINN_Orden_Compra_menu",
        "TINN_Orden_Compra_admin",
        "dashicons-feedback",
        75
    );
}

function TINN_Orden_Compra_admin()
{
    global $wpdb;
    $tabla_jira_oc = $wpdb->prefix . "jira_oc";
    $ordenesCompras = $wpdb->get_results("SELECT * FROM $tabla_jira_oc");
    echo '<div class="wrap"><h1>Lista de Ordenes de Compras</h1>';
    echo '<table class="wp-list-table widefat fixed striped">';
    echo '<thead><tr>';
    echo '<th>Orden</th>';
    echo '<th>Tipo</th>';
    echo '<th>Nombre</th>';
    echo '<th>Apellido</th>';
    echo '<th>Email</th>';
    echo '<th>Código</th>';
    echo '<th>Banco</th>';
    echo '<th>Recibo</th>';
    echo '<th>Fecha</th>';
    echo '<th>Ip</th>';
    echo '<th>Creado el</th>';
    echo '</tr></thead>';
    echo '<tbody id="the-list">';
    foreach ($ordenesCompras as $ordenCompra) {
        $key = esc_textarea($ordenCompra->orden_compra);
        $tipo= esc_textarea($ordenCompra->tipo);
        $nombre= esc_textarea($ordenCompra->nombre);
        $apellido= esc_textarea($ordenCompra->apellido);
        $email= esc_textarea($ordenCompra->email);
        $code = esc_textarea($ordenCompra->code);
        $banco = esc_textarea($ordenCompra->banco);
        $recibo = esc_textarea($ordenCompra->transferencia);
        $fecha = esc_textarea($ordenCompra->fecha);
        $ip = esc_textarea($ordenCompra->ip);
        $creado = esc_textarea($ordenCompra->created_at);
        echo "<tr><td>$key</td>";
        echo "<td>$tipo</td>";
        echo "<td>$nombre</td>";
        echo "<td>$apellido</td>";
        echo "<td>$email</td>";
        echo "<td>$code</td>";
        echo "<td>$banco</td>";
        echo "<td>$recibo</td>";
        echo "<td>$fecha</td>";
        echo "<td>$ip</td><td>$creado</td>";
        echo '</tr>';
    }
    echo '</tbody></table></div>';
}

/**
 * Devuelve la IP del usuario que está visitando la página
 * Código fuente: https://stackoverflow.com/questions/6717926/function-to-get-user-ip-address
 *
 * @return string
 */
function TINN_Obtener_IP_usuario()
{
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED',
        'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED',
        'REMOTE_ADDR') as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                    return $ip;
                }
            }
        }
    }
}


function TINN_Conexion_Jira($ordenCompra)
{
    require_once __DIR__ . "/lib/unirest-php/src/Unirest.php";
    Unirest\Request::auth('Cliente_BBU', 'Nativa2019.');
    Unirest\Request::verifyPeer(false);

    $headers = array(
        'Accept' => 'application/json',
    );

    $url = "https://pac.nativapagos.com/rest/api/2/issue/" . $ordenCompra;

    $response = Unirest\Request::get($url, $headers);

    //echo('<pre>'); var_dump($response); 	echo('</pre>'); die();
    if ($response->code === 200) {
        $respuesta = array(
            'key' => $response->body->key,
            'code' => $response->code,
            'cantidad_Verifone-VX520' => $response->body->fields->customfield_12704,
            'monto_Verifone-VX520' => $response->body->fields->customfield_12808,
            'cantidad_Ingenico-ICT220' => $response->body->fields->customfield_12811,
            'monto_Ingenico-ICT220' => $response->body->fields->customfield_12814,
            'cantidad_Ingenico-IWL20' => $response->body->fields->customfield_12812,
            'monto_Ingenico-IWL20' => $response->body->fields->customfield_12815,
            'cantidad_S90' => $response->body->fields->customfield_12813,
            'monto_S90' => $response->body->fields->customfield_12816,
            'cantidad_PAXD200T' => $response->body->fields->customfield_12719,
            'monto_PAXD200T' => $response->body->fields->customfield_12712,
            'razon' => $response->body->fields->customfield_12904[0],
            'tipo' => $response->body->fields->issuetype->name,
            'descr' => $response->body->fields->description,
        );
    } else {
        $respuesta = array(
            'code' => $response->code,
        );
    }
    //echo("<pre>"); var_dump($respuesta); echo("</pre>"); die();
    return $respuesta;
}



function TINN_Conexion_Jira_Update($tipo, $key, $datos)
{
    require_once __DIR__ . "/lib/unirest-php/src/Unirest.php";
    Unirest\Request::auth('Cliente_BBU', 'Nativa2019.');
    Unirest\Request::verifyPeer(false);
    $headers = array(
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    );

    // Tipo: 1 es TDC y Tipo:0 es Transferencia
    if ($tipo==0) {
        $resultado = " Transferencia Bancaria:  Nombre: " . $datos['nombre'] . " Email: " . $datos['email'] . " Teléfono: " . $datos['telefono'] . " Banco: " . $datos['banco'] . " Código: " . $datos['codigo'] . " Fecha: " . $datos['fecha'] . " Ordenante: " . $datos['ordentante'] ;
    } else{
        $resultado = "Tarjeta de Crédito - Pagalo Fácil:  Código: " .  $datos['codigo'] ;
    }

    $body= '{"update": {"description": [{"set":"'. $resultado .'" }]}}';


    $url = "https://pac.nativapagos.com/rest/api/2/issue/" . $key;
    //$body ='{"update": {"description": [{"set": "Pago Exitoso: Ejecutado por la plataforma Pagalo Facil"}]}}';
    $response = Unirest\Request::put(
        $url,
        $headers,
        $body
    );

    //echo("<pre>"); var_dump( $key, $body, $response); echo("</pre>"); die();
    return $response;
}


function TINN_Formulario_Orden_Compra()
{
    wp_enqueue_style('css-tinn-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
ob_start(); ?>

<main class="page">
  <section class="shopping-cart dark">
    <div id="resumen" class="container">
      <div class="block-heading">
        <h2>Orden de Compra</h2>
        <p> Número de ticket para consultar pedido</p>
      </div>
      <div class="content">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <div class="items">
              <form action="<?php get_the_permalink(); ?>" method="post" class="ordenCompra" id="ordenCompra">
                  <?php wp_nonce_field('graba_orden_compra', 'orden_compra_nonce'); ?>
                  <div class="form-input">
                    <label for='orden_compra'>Ingrese número </label>
                    <input type="text" name="ordenCompra" id="ordenCompra" required>
                  </div>
                  <br>
                  <div class="form-input">
                    <input type="submit" Value="Consultar">
                  </div>
                </form>
            </div>
          </div>
        </div>
      </div>
      </div>
  </section>
</main>
<?php
 wp_enqueue_script('js-tinn-google', 'https://code.jquery.com/jquery-3.2.1.min.js');
    wp_enqueue_script('js-tinn-BB', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
}

function TINN_Formulario_Resultado_Orden_Compra($ordenesCompras)
{
    ob_start(); ?>
<div class="wrap">
  <h1>Datos de la Orden de Compra</h1>
  <div class="col-12">
    <ul class="list-unstyled text-small">
      <li>Orden de Compra: </li> <b> <?php echo strtoupper($ordenesCompras['key']); ?> </b>
      <li>Cliente: </li> <b> <?php echo strtoupper($ordenesCompras['razon']); ?> </b>
      <li>Tipo: </li> <b> <?php echo strtoupper($ordenesCompras['tipo']); ?> </b>
      <li>Descripción: </li> <b> <?php echo strtoupper($ordenesCompras['descr']); ?> </b>
      <li>Cantidad: </li> <b> <?php echo strtoupper($ordenesCompras['cantidad']); ?> </b>
      <li>Monto: </li> <b> $ <?php echo strtoupper($ordenesCompras['monto']); ?> </b>
    </ul>
  </div>
</div>
<?php
}

function TINN_Formulario_Resultado_Pagalo_Facil_Negada($result)
{
    //echo('<pre>'); var_dump($result); echo('</pre>');
    ob_start(); ?>
<div class="wrap">
  <h1>Resultados de la Operación</h1>
  <div class="col-12">
    <ul class="list-unstyled text-small">
      <li>Estatus: </li> <b> <?php echo strtoupper($result->Status); ?> </b>
      <li>Nombre: </li> <b> $ <?php echo strtoupper($result->Name); ?> </b>
      <li>Apellido: </li> <b> $ <?php echo strtoupper($result->LastName); ?> </b>
      <li>Email: </li> <b> $ <?php echo strtoupper($result->Email); ?> </b>
      <li>Monto: </li> <b> <?php echo strtoupper($result->Amount); ?> </b>
      <li>Tipo de tarjeta: </li> <b> $ <?php echo strtoupper($result->CardType); ?> </b>
      <li>Descripción: </li> <b> <?php echo strtoupper($result->RespText); ?> </b>
      <li>Código de respuesta: </li> <b> <?php echo strtoupper($result->RespCode); ?> </b>
      <li>Código de Operación: </li> <b> <?php echo strtoupper($result->CODOPER); ?> </b>
      <li>Fecha: </li> <b> $ <?php echo strtoupper($result->Date); ?> </b>
      <li>Hora: </li> <b> $ <?php echo strtoupper($result->Time); ?> </b>
    </ul>
  </div>
</div>
<?php
}

function TINN_Formulario_Resultado_Jira($respuesta)
{
    //var_dump($respuesta); die();

    if ($respuesta->code==204) {
        ob_start(); ?>
        <div class="wrap">
          <h1>Pago Exitoso</h1>
          <div class="col-12">
            <ul class="list-unstyled text-small">
              <li>Gracias por su pago generando Factura, por favor espere... </li>
            </ul>
          </div>
        </div>
    <?php } else {
        ob_start(); ?>
        <div class="wrap">

          <div class="col-12">

            <div class="alert alert-danger" role="alert">Error en Conexión: Estamos presentando problemas con la comunicación con la Plataforma Jira. Intente nuevamente más tarde.</div>
          </div>
        </div>
<?php
 TINN_Formulario_Orden_Compra();
    }
}

function TINN_Formulario_Resultado_Jira_Orden_Compra_No_Valida()
{
   ob_start(); ?>
        <div class="alert alert-danger" role="alert"> Orden de Compra No Válida</div>
    <?php
    TINN_Formulario_Orden_Compra();
}

function TINN_Insertar_BD_Orden_Compra($registro)
{
    global $wpdb;
    $tabla_jira_oc = $wpdb->prefix . "jira_oc";
    $wpdb->insert(
        $tabla_jira_oc,
        array(
            'orden_compra' => $registro['orden_compra'],
            'tipo' => $registro['tipo'],
            'nombre' => $registro['nombre'],
            'apellido' => $registro['apellido'],
            'email' => $registro['email'],
            'code' => $registro['code'],
            'banco' => $registro['banco'],
            'transferencia' => $registro['transferencia'],
            'fecha' => $registro['fecha'],
            'ip' => TINN_Obtener_IP_usuario(),
            'created_at' => date('Y-m-d H:i:s'),



        )
    );
}



function TINN_Conexion_Odoo()
{
    $url = "https://test85.odoo.com";
    $db = "test85";
    $username = "oscarsomostinn@gmail.com";
    $password = "zK219upx";

    require_once __DIR__ . "/lib/ripcord/ripcord.php";
    /*$info = ripcord::client('https://test85.odoo.com')->start();
    list($url, $db, $username, $password) =
    array($info['host'], $info['database'], $info['user'], $info['password']);*/

    $common = ripcord::client("$url/xmlrpc/2/common");
    /*	var_dump($common->version());
        die();*/
    $uid = $common->authenticate($db, $username, $password, array());
    var_dump($uid);
    die();


    //return $response;
}


function TINN_Pagalo_Facil($post)
{
     /* Procesar Pagalo facil */

            //Buscamos la OC nuevamente para tener el monto correcto
            $ordenesCompras = TINN_Conexion_Jira(sanitize_text_field($post['key']));

            $monto = ($ordenesCompras['monto_Ingenico-ICT220'] + $ordenesCompras['monto_Ingenico-IWL20']
                    + $ordenesCompras['monto_Verifone-VX520'] + $ordenesCompras['monto_S90']
                    + $ordenesCompras['monto_PAXD200T']);
            $key = $ordenesCompras['key'];
            $concepto = strtoupper($ordenesCompras['descr']);

            $hash = sanitize_text_field($post['NumeroTarjeta']) . sanitize_text_field($post['SecurityCode']) . sanitize_text_field($post['USR']);
            $cclw = '0540A772DC7E1FA1EB170B9DA1C2060E6EB736B95270E3E783580C835F5E83331208CBC111627601EA03C62CDAEC4CC90CC203A161E8BC17E861650165C6173E';
            $data = array(
                "CCLW" => $cclw,
                "txType" => 'SALE',
                "CMTN" => $monto,
                "CDSC" => $concepto,
                "CCNum" => sanitize_text_field($post['NumeroTarjeta']),
                "ExpMonth" => sanitize_text_field($post['ExpMes']),
                "ExpYear" => sanitize_text_field($post['ExpAno']),
                "CVV2" => sanitize_text_field($post['SecurityCode']),
                "Name" => strtoupper(sanitize_text_field($post['Nombres'])),
                "LastName" => strtoupper(sanitize_text_field($post['Apellidos'])),
                "Email" => sanitize_text_field($post['USR']),
                "Address" => strtoupper(sanitize_text_field($post['Direccion'])),
                "Tel" => sanitize_text_field($post['Telefono']),
                "SecretHash" => hash('sha512', $hash),
            );


            $postR = "";
            foreach ($data as $mk => $mv) {
                $postR .= "&" . $mk . "=" . $mv;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://sandbox.paguelofacil.com/rest/ccprocessing/");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Accept: */*'));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postR);
            $result = curl_exec($ch);
            $result = json_decode($result);

            if (empty($result->Status)) {
                $resultado = "";
            } else {
                $resultado = $result->Status;
            }

 //echo('<pre>'); var_dump($data, $result); echo('</pre>'); die();

            switch ($resultado) {
                case "Approved":
                    $datos['codigo']=$result->CODOPER;
                    $tipo=1;
                    $respuesta= TINN_Conexion_Jira_Update($tipo, $key, $datos);

                    // Arreglo con los datos para almacenar en la BD
                    $registro['orden_compra']=$key;
                    $registro['tipo']="TDC";
                    $registro['nombre']=strtoupper(sanitize_text_field($post['Nombres']));
                    $registro['apellido']=strtoupper(sanitize_text_field($post['Apellidos']));
                    $registro['email']=sanitize_text_field($post['USR']);
                    $registro['code']=$result->CODOPER;
                    $registro['banco']=NULL;
                    $registro['transferencia']=NULL;
                    $registro['fecha']= NULL;
                    //Registrar en BD la Consulta
                    TINN_Insertar_BD_Orden_Compra($registro);

                    //Mostar Resultados de la Operacion
                    TINN_Formulario_Resultado_Jira($respuesta);
                    break;
                case "Declined":
                    TINN_Formulario_Resultado_Pagalo_Facil_Negada($result);
                    break;
                    default:
                    //$respuesta= TINN_Conexion_Jira_Update($key);
                        echo "<div class='alert alert-danger' role='alert'> Error de comunicación con la Pasarela de Pago. Intentele más tarde</div><br>";
                        TINN_Formulario_Orden_Compra();
            }


}


/**
 * Procesa el Método de Pago por Transferencia Bancaria
 *
 *
 */
function TINN_Transferencia($post)
{
  //  echo('<pre>'); var_dump($post);   echo('</pre>'); die();

  // These files need to be included as dependencies when on the front end.
	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
	require_once( ABSPATH . 'wp-admin/includes/media.php' );

	// Let WordPress handle the upload.
	// Remember, 'my_image_upload' is the name of our file input in our form above.
	$attachment_id = media_handle_upload( 'captura', 0);

  //var_dump($url, $attachment_id); die('eeee');


	if ( is_wp_error( $attachment_id ) ) {
    //var_dump($attachment_id);
		die("error al subir archivo");// There was an error uploading the image.
	} else {
      $urlImagen= wp_get_attachment_url( $attachment_id );
//	die("subio archivo");	// The image was uploaded successfully!
	}


    $key = sanitize_text_field($post['key']);
    $datos['nombre'] = sanitize_text_field($post['Nombres'])." ".sanitize_text_field($post['Apellidos']);
    $datos['email']  = sanitize_text_field($post['USR']);
    $datos['telefono'] = sanitize_text_field($post['Telefono']);
    $datos['banco'] = sanitize_text_field($post['Banco']);
    $datos['codigo'] = sanitize_text_field($post['Transferencia']);
    $datos['fecha']= sanitize_text_field($post['Fecha']);
    $datos['ordentante'] = sanitize_text_field($post['Ordenante']);
    $datos['imagen'] = $attachment_id ;
    $tipo=0;
    $respuesta= TINN_Conexion_Jira_Update($tipo, $key, $datos);
    // Arreglo con los datos para almacenar en la BD
    $registro['orden_compra']=$key;
    $registro['tipo']="Transferencia";
    $registro['nombre']=strtoupper(sanitize_text_field($post['Nombres']));
    $registro['apellido']=strtoupper(sanitize_text_field($post['Apellidos']));
    $registro['email']=sanitize_text_field($post['USR']);
    $registro['code']=$result->CODOPER;
    $registro['banco']= strtoupper(sanitize_text_field($post['Banco']));
    $registro['transferencia']= strtoupper(sanitize_text_field($post['Transferencia']));
    $registro['fecha']= date($post['Fecha']);
    $registro['imagen'] = $attachment_id ;
    //Registrar en BD la Consulta
    TINN_Insertar_BD_Orden_Compra($registro);
    TINN_Formulario_Resultado_Jira($respuesta);

}


/**
 * Muestra la lista de productos y los Métodos de Pago
 *
 *
 */
function TINN_Formulario_Orden_Compra_Pagalo_Facil($ordenesCompras)
{
    wp_enqueue_style('css-tinn-bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
    wp_enqueue_style('css-tinn-fonts', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('css_intl_tinn', plugins_url("/lib/intl-tel-input/build/css/intlTelInput.css", __FILE__));
    wp_enqueue_style('css_tinn', plugins_url('style.css', __FILE__));
    ob_start(); ?>
<main class="page">
  <section class="shopping-cart dark">
    <div id="resumen" class="container">
      <div class="block-heading">
        <h2>Orden de Compra</h2>
        <p>Lista de los Productos Adquiridos</p>
      </div>
      <div class="content">
        <div class="row">
          <div class="col-md-8 col-lg-8">
            <div class="items">
              <br>
              <p>Cliente: <b><?php echo strtoupper($ordenesCompras['razon']); ?> </b> <br>
              Ticket: <b><?php echo strtoupper($ordenesCompras['key']); ?> </b> <br>
              Tipo: <b><?php echo strtoupper($ordenesCompras['tipo']); ?> </b> <br></p>
              <br>
            </div>
          </div>
        </div>
      </div>
      <div class="content">
        <div class="row">
          <div class="col-md-12 col-lg-8">
            <div class="items">
              <?php if ($ordenesCompras['cantidad_Ingenico-ICT220']) { ?>
              <div class="product">
                <div class="row">
                  <div class="col-md-3">
                    <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/ingenico-ict22.jpg", __FILE__); ?>>
                  </div>
                  <div class="col-md-8">
                    <div class="info">
                      <div class="row">
                        <div class="col-md-8 product-name">
                          <div class="product-name">
                            <p>Ingenico ICT22</p>
                            <div class="product-info">
                              <div>Cantidad: <span class="value"> <?php echo $ordenesCompras['cantidad_Ingenico-ICT220']; ?> </span></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 price">
                          <span>$<?php echo $ordenesCompras['monto_Ingenico-ICT220']; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

              <?php if ($ordenesCompras['cantidad_Ingenico-IWL20']) { ?>
              <div class="product">
                <div class="row">
                  <div class="col-md-3">
                    <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/Ingenico-IWL220.jpg", __FILE__); ?>>
                  </div>
                  <div class="col-md-8">
                    <div class="info">
                      <div class="row">
                        <div class="col-md-8 product-name">
                          <div class="product-name">
                            <p>Ingenico IWL220 3G</p>
                            <div class="product-info">
                              <div>Cantidad: <span class="value"> <?php echo $ordenesCompras['cantidad_Ingenico-IWL20']; ?> </span></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 price">
                          <span>$<?php echo $ordenesCompras['monto_Ingenico-IWL20']; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

              <?php if ($ordenesCompras['cantidad_Verifone-VX520']) { ?>
              <div class="product">
                <div class="row">
                  <div class="col-md-3">
                    <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/Verifone.jpg", __FILE__); ?>>
                  </div>
                  <div class="col-md-8">
                    <div class="info">
                      <div class="row">
                        <div class="col-md-8 product-name">
                          <div class="product-name">
                            <p>Ingenico IWL220 3G</p>
                            <div class="product-info">
                              <div>Cantidad: <span class="value"> <?php echo $ordenesCompras['cantidad_Verifone-VX520']; ?> </span></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 price">
                          <span>$<?php echo $ordenesCompras['monto_Verifone-VX520']; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

              <?php if ($ordenesCompras['cantidad_S90']) { ?>
              <div class="product">
                <div class="row">
                  <div class="col-md-3">
                    <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/Verifone.jpg", __FILE__); ?>>
                  </div>
                  <div class="col-md-8">
                    <div class="info">
                      <div class="row">
                        <div class="col-md-8 product-name">
                          <div class="product-name">
                            <p>S90</p>
                            <div class="product-info">
                              <div>Cantidad: <span class="value"> <?php echo $ordenesCompras['cantidad_S90']; ?> </span></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 price">
                          <span>$<?php echo $ordenesCompras['monto_S90']; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>

              <?php if ($ordenesCompras['cantidad_PAXD200T']) { ?>
              <div class="product">
                <div class="row">
                  <div class="col-md-3">
                    <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/Verifone.jpg", __FILE__); ?>>
                  </div>
                  <div class="col-md-8">
                    <div class="info">
                      <div class="row">
                        <div class="col-md-8 product-name">
                          <div class="product-name">
                            <p>PAXD200T</p>
                            <div class="product-info">
                              <div>Cantidad: <span class="value"> <?php echo $ordenesCompras['cantidad_PAXD200T']; ?> </span></div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 price">
                          <span>$<?php echo $ordenesCompras['monto_PAXD200T']; ?></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php } ?>
            </div>
          </div>
          <div class="col-md-12 col-lg-4">
            <div class="summary">
              <h3>Resumen</h3>
                <?php if ($ordenesCompras['monto_Ingenico-ICT220']) { ?>
                  <div class="summary-item"><span class="text">Ingenico ICT22</span><span class="price">$<?php echo $ordenesCompras['monto_Ingenico-ICT220']; ?></span></div>
                <?php } ?>
                <?php if ($ordenesCompras['monto_Ingenico-IWL20']) { ?>
                  <div class="summary-item"><span class="text">IngenicoiWL220 3G</span><span class="price">$<?php echo $ordenesCompras['monto_Ingenico-IWL20']; ?></span></div>
                <?php } ?>
                <?php if ($ordenesCompras['monto_Verifone-VX520']) { ?>
                  <div class="summary-item"><span class="text">Verifone 520</span><span class="price">$<?php echo $ordenesCompras['monto_Verifone-VX520']; ?></span></div>
                <?php } ?>
                <?php if ($ordenesCompras['monto_S90']) { ?>
                  <div class="summary-item"><span class="text">S90</span><span class="price">$<?php echo $ordenesCompras['monto_S90']; ?></span></div>
                <?php } ?>
                <?php if ($ordenesCompras['monto_PAXD200T']) { ?>
                  <div class="summary-item"><span class="text">PAXD200T</span><span class="price">$<?php echo $ordenesCompras['monto_PAXD200T']; ?></span></div>
                <?php } ?>
                <br>

              <div class="summary-item"><span class="text">TOTAL</span><span class="text price">$<?php echo ($ordenesCompras['monto_Ingenico-ICT220']+$ordenesCompras['monto_Ingenico-IWL20']+$ordenesCompras['monto_Verifone-VX520']+$ordenesCompras['monto_S90']+$ordenesCompras['monto_PAXD200T']); ?></span></div>
              <br>
              <!--button id="checkout" type="button" class="btn btn-primary btn-lg btn-block">Pagar Productos</button-->
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- INICO -->

    <div id="metodo-pago" class="container">
      <div class="block-heading">
        <h2>Métodos de Pago</h2>
        <!--p>Seleccione la forma de Pago</p-->
          <br>
        <!--p>Monto Total a Cancelar <b>$<?php echo ($ordenesCompras['monto_Ingenico-ICT220']+$ordenesCompras['monto_Ingenico-IWL20']+$ordenesCompras['monto_Verifone-VX520']+$ordenesCompras['monto_S90']+$ordenesCompras['monto_PAXD200T']); ?></b> </p>
        <br>
        <button id="productos" type="button" class="btn">Regresar a los Productos</button-->
      </div>
      <div class="content">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <div class="items">
              <!-- Acordeon -->

              <div id="accordion" role="tablist" aria-multiselectable="true">
                <div class="card">
                  <div class="card-header" role="tab" id="headingOne">
                    <div class="mb-0">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                        <h3>Tarjeta de Crédito/Débito</h3>
                      </a>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>
                  </div>

                  <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="">
                    <div class="card-block">
                      <!-- FORMULARIO TDC -->
                      <section class="payment-form dark">
                        <div class="container">
                          <div class="block-heading">
                            <div class="icon-container">
                              <h4>Tarjetas Aceptadas</h4>
                              <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/VisaMC.png", __FILE__); ?>>
                            </div>
                          </div>
                          <form action="<?php get_the_permalink(); ?>" method="post" class="ordenCompraPagoFacil" id="ordenCompraPagoFacil">
                            <?php wp_nonce_field('graba_orden_compra_pago', 'orden_compra_pago_nonce'); ?>
                            <div class="card-details">
                              <h3 class="title">Datos del Titular</h3>
                              <div class="row">
                                <div class="form-group col-sm-6">
                                  <label for="nombre">Nombre</label>
                                  <input id="cname" name="Nombres" type="text" required maxlength="25" class="form-control" placeholder="Ej. Oscar" aria-label="Ej. Oscar" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="apellido">Apellido</label>
                                  <input id="cnlastname" name="Apellidos" type="text" required maxlength="25" class="form-control" placeholder="Ej. Velasquez" aria-label="Ej. Velasquez" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="email">Email</label>
                                  <input type="email" id="email" name="USR" required maxlength="100" class="form-control" placeholder="Ej. abc@def.com" aria-label="Ej. abc@def.com" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="telefono">Teléfono</label>
                                  <input type="tel" id="Telefono" name="Telefono" required maxlength="16" class="form-control" placeholder="Ej. +582121234567" aria-label="Ej. +582121234567" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-12">
                                  <label for="direccion">Dirección</label>
                                  <input type="text" id="adr" name="Direccion" required maxlength="100" class="form-control" placeholder="Ej. Caracas, Venezuela" aria-label="Ej. Caracas, Venezuela" aria-describedby="basic-addon1">
                                </div>
                              </div>
                            </div>

                            <div class="card-details">
                              <h3 class="title">Detalles de la Tarjeta de Crédito/Débito</h3>
                              <div class="row">
                                <div class="form-group col-sm-8">
                                  <label for="card-holder">Número de la Tarjeta</label>
                                  <input type="text" id="ccnum" name="NumeroTarjeta" required pattern="^(?:4\d([\- ])?\d{6}\1\d{5}|(?:4\d{3}|5[1-5]\d{2}|6011)([\- ])?\d{4}\2\d{4}\2\d{4})$" class="form-control" placeholder="Ingresar números sin guiones ni espacios"
                                    aria-label="Sin Guiones ni espacios" aria-describedby="basic-addon1">

                                </div>
                                <div class="form-group col-sm-4">
                                  <label for="cvc">Código de Seguridad</label>
                                  <input type="number" id="cvv" name="SecurityCode" min="100" max="999" required class="form-control" placeholder="CVC" aria-label="Card Holder" aria-describedby="basic-addon1">
                                </div>

                                <div class="form-group col-sm-8">
                                  <label for="ExpMes">Mes de Expiración</label>
                                  <select name="ExpMes" class="custom-select"required>
                                    <option value="01">Enero</option>
                                    <option value="02">Febrero</option>
                                    <option value="03">Marzo</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Mayo</option>
                                    <option value="06">Junio</option>
                                    <option value="07">Julio</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                  </select>
                                </div>

                                <div class="form-group col-sm-4">
                                  <label for="expyear">Año de Expiración</label>
                                  <input type="number" class="form-control" id="expyear" name="ExpAno" value="<?php echo date("Y")?>"
                                  min="<?php echo date("Y")?>" required aria-label="YY" aria-describedby="basic-addon1">
                                </div>

                                <div class="form-group col-sm-12">
                                  <div class="input-group expiration-date">
                                    <input type="checkbox" id="aceptacion" name="aceptacion" value="1" required> Entiendo y acepto los <a href="/terminos-y-condiciones/" target="_blank"> Términos y Condiciones </a>
                                  </div>
                                </div>
                                <input id="key" type="hidden" name="key" value="<?php echo strtoupper($ordenesCompras['key']); ?>">
                                <div class="form-group col-sm-12">
                                  <input type="submit" value="Proceder" class="btn btn-primary btn-block">
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </section>
                      <!-- FIN FORM TDC-->
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" role="tab" id="headingTwo">
                    <div class="mb-0">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                        <h3>Transferencia Bancaria</h3>
                      </a>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </div>
                  </div>
                  <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false">
                    <div class="card-block">
                      <!-- FORMULARIO Transferencia -->
                      <section class="payment-form dark">
                        <div class="container">
                          <div class="block-heading">
                            <div class="icon-container">
                              <img class="img-fluid mx-auto d-block image" src=<?php echo plugins_url("assets/img/wire-transfer-bank.gif", __FILE__); ?>>
                            </div>
                          </div>
                          <form action="<?php get_the_permalink(); ?>" method="post" class="ordenCompraTransferencia" id="ordenCompraTransferencia" enctype="multipart/form-data">
                            <?php wp_nonce_field('graba_orden_compra_transferencia', 'orden_compra_transferencia_nonce'); ?>
                            <div class="card-details">
                              <h3 class="title">Datos de la Transferencia</h3>
                              <div class="row">
                                <div class="form-group col-sm-6">
                                  <label for="card-holder">Nombre</label>
                                   <input id="cname" name="Nombres" type="text" required maxlength="25" class="form-control" placeholder="Ej. Oscar" aria-label="Ej. Oscar" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="">Apellido</label>
                                 <input id="cnlastname" name="Apellidos" type="text" required maxlength="25" class="form-control" placeholder="Ej. Velasquez" aria-label="Ej. Velasquez" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="card-number">Email</label>
                                   <input type="email" id="email" name="USR" required maxlength="100" class="form-control" placeholder="Ej. abc@def.com" aria-label="Ej. abc@def.com" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="cvc">Teléfono</label>
                                  <input type="tel" id="movil" name="Telefono" required maxlength="16" class="form-control" placeholder="Ej. +582121234567" aria-label="Ej. +582121234567" aria-describedby="basic-addon1">
                                </div>

                                <div class="form-group col-sm-6">
                                  <label for="banco">Banco</label>
                                 <input id="banco" name="Banco" type="text" required maxlength="100" class="form-control" placeholder="Ej. Banesco" aria-label="Banesco" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="codigo">Código de la Transferencia</label>
                                 <input id="transferencia" name="Transferencia" type="text" required maxlength="50" class="form-control" placeholder="Ej. 1234567890" aria-label="Ej. 1234567890" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="Fecha">Fecha</label>
                                  <input id="fecha" name="Fecha" type="date" class="form-control" placeholder="Fecha" aria-label="Fecha" aria-describedby="basic-addon1">
                                </div>
                                <div class="form-group col-sm-6">
                                  <label for="Ordenante">Ordenante</label>
                                 <textarea  id="Ordenante" name="Ordenante" rows="4" cols="50"> </textarea>
                                 <small id="passwordHelpBlock" class="form-text text-muted">Identifica los titulares de la cuenta que emite el pago (se deben incluir todos los firmantes en la cuenta)</small>
                                </div>

                                <div class="form-group col-sm-6">
                                  <label for="Ordenante">Comprobante</label>
                                 <input type="file" name="captura" id="captura"  multiple="false" accept="application/pdf, image/*" />
                                 <small id="passwordHelpBlock" class="form-text text-muted">Subir imagen o pdf de soporte de la transferencia</small>
                               </div>

                                 <div class="form-group col-sm-12">
                                  <div class="input-group expiration-date">
                                    <input type="checkbox" id="aceptacion" name="aceptacion" value="1" required> Entiendo y acepto los <a href="/terminos-y-condiciones/" target="_blank"> Términos y Condiciones </a>
                                  </div>
                                </div>
                                <input id="key" type="hidden" name="key" value="<?php echo strtoupper($ordenesCompras['key']); ?>">
                                <div class="form-group col-sm-12">
                                  <input type="submit" value="Proceder" class="btn btn-primary btn-block">
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </section>
                      <!-- FIN Transferencia -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- FinAcordeon -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- FIN -->
  </section>
</main>
<?php
    wp_enqueue_script('js-tinn-google', 'https://code.jquery.com/jquery-3.2.1.min.js');
    wp_enqueue_script('js-tinn-BB', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
    wp_enqueue_script('js_intl_tinn', plugins_url('lib/intl-tel-input/build/js/intlTelInput-jquery.min.js', __FILE__));
    wp_enqueue_script('js_intl_codigo_tinn', plugins_url('lib/intl-tel-input/build/js/codigo.js', __FILE__));
}
